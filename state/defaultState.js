//load library that u will use here
let send = require('../lib/sendMsg.js')
let firebase = require('../lib/firebaseLibs.js')
let gtion= require('../lib/gtionLibs.js')

//load other state related to this state
//Warning : Loading other state logic have to use delay
let greeting = null;
let dungeon = null;
let floor = null;
let onMeetEnemy = null;
let town = null;
let arena = null;
setTimeout(()=>{
    greeting = require('../state/greeting.js')
    dungeon = require('../state/dungeon.js')
    floor = require('../state/floor.js')
    onMeetEnemy = require('../state/onMeetEnemy.js')
    town = require('../state/town.js')
    arena = require('../state/arena.js')
}, 100);

module.exports = {
    unknownAnswer : function(room_id){
        send.Txt(room_id , "Maaf, jawabanmu tidak dapat dikenali\n.\nPanggil bantuan dengan ketik '/help'" );
    },
    askHelp : function(room_id){
        send.Txt(room_id , "Ini Fitur Bantuan\n/menu\tmenampilkan menu saat ini\n/status\tmenampilkan status karakter");
    },
    menu : function(payloadBody){
        firebase.getValue('player-state/'+payloadBody.userId , (state)=>{
            console.log(state ,gtion.stateToInt(state));
            switch (gtion.stateToInt(state)) {
                case 1:
                    town.init(payloadBody.room_id , payloadBody.userId);
                    break;
                case 2:
                    dungeon.init(payloadBody.room_id , payloadBody.userId);
                    break;
                case 3:
                    floor.init(payloadBody.room_id , payloadBody.userId);
                    break;
                case 4:
                    onMeetEnemy.init(payloadBody.room_id , payloadBody.userId);
                    break;
                case 5:
                    arena.init(payloadBody.room_id , payloadBody.userId);
                    break;
                default:
                    greeting.init(payloadBody.room_id , payloadBody.userId , payloadBody.name);
            }
        });
    },
    status : function(payloadBody){
        firebase.getValue('player-state/'+payloadBody.userId , (state)=>{
            switch (gtion.stateToInt(state)) {
                case 5:{
                            let arenaReff = gtion.stateToKey(state)[1]
                            firebase.getValue('arena/'+arenaReff+'/player/'+payloadBody.userId , (character) =>{
                                let statusValue = gtion.fullStatusToString(character.name ,character.currentHp ,character.currentEnergy , character.maxHp , character.maxEnergy,character.attack, character.accuracy , character.agility)
                                let payload = {
                                    'text': 'Status Karakter-mu :\n'+statusValue,
                                    'buttons': []
                                }
                                send.Btn(payloadBody.room_id,payload)
                            });
                        }
                    break;
                default:
                    firebase.getValue('player/'+payloadBody.userId+'/character' , (character)=>{
                        if(character == null){
                            send.Txt(payloadBody.room_id,'Akunmu belum ter-registrasi')
                            return;
                        }
                        let statusValue = gtion.fullStatusToString(character.name ,character.status.currentHp ,character.status.currentEnergy , character.status.maxHp , character.status.maxEnergy,character.status.attack, character.status.accuracy , character.status.agility)
                        let payload = {
                            'text': 'Status Karakter-mu :\n'+statusValue+"\n\nLevel\t: "+character.level+"\nExperience\t: "+character.experience+"/"+gtion.getRequireLevelUpExp(character.level)+"\nAscension\t: "+character.ascension+"\nGunakan 'Ascension' untuk meningkatkan status-mu di 'Celestial Palace'.",
                            'buttons': []
                        }
                        send.Btn(payloadBody.room_id,payload)
                    });
            }
        });

    }

}
