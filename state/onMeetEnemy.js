//load library that u will use here
let firebase = require('../lib/firebaseLibs.js')
let send = require('../lib/sendMsg.js')
let gtion = require('../lib/gtionLibs.js')

//load other state related to this state
//Warning : Loading other state logic have to use delay
let defaultState = null
//let dungeon = null;
let floor = null
let arena = null
setTimeout(()=>{
  defaultState = require('../state/defaultState.js')
 // dungeon = require('../state/dungeon.js')
  floor = require('../state/floor.js')
  arena = require('../state/arena.js')
}, 100);


module.exports = {
    init : function(room_id , userId){
      //message when enter this state
      firebase.setValue('player-state/'+userId , 'onMeetEnemy');
      firebase.getValue('player-variables/'+userId+'/meet-enemy' , (meetEnemy) =>{
          let monsterData = []
          let keys = gtion.getVariableKey(meetEnemy);
          for(let i = 0 ; i < keys.length; i++){
              if( meetEnemy[keys[i]].monsterPath == null)
                continue;

              monsterData.push({ monsterPath : meetEnemy[keys[i]].monsterPath , monsterLevel : meetEnemy[keys[i]].monsterLevel})
          }
          this.askDecision(room_id ,userId, meetEnemy.xpGain , monsterData)
      })

    },
    mainFunction : function (payloadBody){ // get value from database and return to callback
        // action when user send message
        if(payloadBody.msg === '/fight'){
            arena.initBattle(payloadBody.room_id , payloadBody.userId);
        }else if(payloadBody.msg ==='/back'){
            firebase.getValue('player/'+payloadBody.userId+'/character/level' , (level)=>{
                gtion.decreaseExperience(payloadBody.room_id , payloadBody.userId , gtion.getRetreatPenalty(level));
                send.Txt(payloadBody.room_id , "Kamu kabur, kamu terkena pinalti "+ gtion.getRetreatPenalty(level)+" Experience",()=>{floor.init(payloadBody.room_id , payloadBody.userId);});
            })
            firebase.deleteValue('player-variables/'+payloadBody.userId+'/meet-enemy');
        }else{
            defaultState.unknownAnswer(payloadBody.room_id);
        }
    },
    askDecision : function( room_id ,userId, xpGain , monsterData){

        let enemyStatus = [];
        for(let i = 0 ; i < monsterData.length; i++){
            firebase.getValue(monsterData[i].monsterPath , (monster)=>{
                //  ,
                 let mStats = gtion.statusToString(monster.status.maxHp , monster.status.maxEnergy , monster.status.maxHp , monster.status.maxEnergy , monster.status.attack , monster.status.accuracy , monster.status.agility);
                 if(monsterData[i].monsterLevel != monster.baseLevel){
                     mStats += "\n\nPeringatan : Data mungkin tidak akurat.";
                 }
                 enemyStatus.push({mStats : mStats, name : monster.name+" Lv."+monsterData[i].monsterLevel , image : monster.image} );

                 if(enemyStatus.length == monsterData.length){
                     this.askDecision2(room_id , userId , xpGain , enemyStatus);
                 }
            })
        }


    },
    askDecision2 : function(room_id , userId , xpGain, enemyStatus){
        let cards = []
        for(let i = 0 ; i < enemyStatus.length; i++){
            cards.push( gtion.carouselPostbackData(enemyStatus[i].image, enemyStatus[i].name , enemyStatus[i].mStats , [] ) );
        }
        let payloadC = {
            'cards' : cards
        }

        send.Carousel(room_id , payloadC , ()=>{
            let options = [ gtion.buttonPostbackData('Fight' , '/fight') , gtion.buttonPostbackData('Retreat (Penalty 5% Max Exp)' , '/back')  ]
            let payload = {
                'text': "Kamu bertemu dengan monster apakah kamu ingin melawannya?",
                'buttons': options
            }
            send.Btn(room_id,payload)
        })
    }
}
