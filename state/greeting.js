let firebase = require('../lib/firebaseLibs.js')
let send = require('../lib/sendMsg.js')
let gtion = require('../lib/gtionLibs.js')

//Warning : Loading other state logic have to use delay
let loginRegist = null
setTimeout(()=>{
  loginRegist = require('../state/loginRegister.js')
}, 100);


module.exports = {
  init : function(room_id, userId , name){
    firebase.deleteValue('player-state/'+userId );
    this.sendGreeting(room_id, name);
  },
  mainFunction : function (payloadBody ){ // get value from database and return to callback

    if(payloadBody.msg === '/login'){
      loginRegist.initLogin(payloadBody.room_id,payloadBody.userId);
    }else if(payloadBody.msg === '/register'){
      loginRegist.initRegist(payloadBody.room_id, payloadBody.userId);
    }else{
      this.sendGreeting(payloadBody.room_id, payloadBody.name);
    }

  },
  sendGreeting:function (room_id, name) {
    let pesan = 'Halo ' + name + ' ! Selamat datang di Adventure Dungeon !'
    let payload = {
        'text': pesan,
        'buttons': [
            gtion.buttonPostbackData('Login','/login'),gtion.buttonPostbackData('Register','/register'),gtion.buttonPostbackData('Help','/help')
        ]
    }
    send.Btn(room_id,payload)
  }
}
