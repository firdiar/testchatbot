//load library that u will use here
let firebase = require('../lib/firebaseLibs.js')
let send = require('../lib/sendMsg.js')
let gtion = require('../lib/gtionLibs.js')

//load other state related to this state
//Warning : Loading other state logic have to use delay
let defaultState = null
let floor = null
let town = null
setTimeout(()=>{
  defaultState = require('../state/defaultState.js')
  floor = require('../state/floor.js')
  town = require('../state/town.js')
}, 100);


module.exports = {
  init : function(room_id , userId){
      //message when enter this state
      firebase.setValue('player-state/'+userId , 'dungeon');
      firebase.getValue('player-variables/'+userId+'/dungeon/highestFloor', (highestFloor)=>{

          let options = [ gtion.buttonPostbackData('Floor '+highestFloor , '/highest-floor') ]
          if(highestFloor != 1){
              options.push(gtion.buttonPostbackData('Select Floor' , '/select-floor'))
          }
          options.push(gtion.buttonPostbackData('Back' , '/back'))

          let payload = {
              'text': '𝗗𝘂𝗻𝗴𝗲𝗼𝗻\nKamu berada di dungeon, silakan pilih lantai yang kamu ingin jelajahi',
              'buttons': options
          }
          send.Btn(room_id,payload)
      })
  },
  mainFunction : function (payloadBody , state){

    switch (payloadBody.msg) {
        case '/highest-floor': //Init floor
            firebase.getValue('player-variables/'+payloadBody.userId+'/dungeon/highestFloor', (highestFloor)=>{
                send.Txt(payloadBody.room_id , "Menuju Lantai : "+highestFloor); this.gotoFloor(payloadBody.room_id ,payloadBody.userId , highestFloor , highestFloor);
            });
            break;
        case '/select-floor': // select desired floor
            firebase.setValue('player-state/'+payloadBody.userId , 'dungeon-1');
            firebase.getValue('player-variables/'+payloadBody.userId+'/dungeon/highestFloor', (highestFloor)=>{
                send.Txt(payloadBody.room_id , "Pilih lantai yang dituju ( 1 - "+highestFloor+" )");
            });
            break;
        case '/back': //Init town
            town.init(payloadBody.room_id , payloadBody.userId);
            break;
        default:{
            let floorNumber = parseInt(payloadBody.msg)
            if(state != 'dungeon-1' || isNaN(floorNumber)) defaultState.unknownAnswer(payloadBody.room_id);
            else this.selectFloor(payloadBody.room_id , payloadBody.userId ,floorNumber);
        }
    }

  }

  ,
  selectFloor:function(room_id , userId , floorNumber){

      firebase.getValue('player-variables/'+userId+'/dungeon/highestFloor', (highestFloor)=>{
          if(floorNumber > highestFloor || floorNumber < 0){
              send.Txt(room_id , "Maaf lantai tidak bisa diakses!"+"\n.\nPilih lantai yang dituju ( 1 - "+highestFloor+" )");
          }else{
              send.Txt(room_id , "Menuju Lantai : "+floorNumber);
              //Init floor
              this.gotoFloor(room_id ,userId , floorNumber , highestFloor);
          }
      });

  },
  gotoFloor:function(room_id ,userId , floorNumber , highestFloor){

      firebase.getValue('floor/'+ gtion.numberToID( floorNumber , "floor-" , "0000") , (value)=>{
          if(value == null){
              send.Txt(room_id , "Lantai "+floorNumber+" saat ini belum tersedia, tolong tunggu update selanjutnya",()=>{this.init(room_id , userId);});
          }else{
              let progress = 0;
              if(floorNumber != highestFloor)
                progress = 80;

              let payload = {floor : floorNumber , progress : progress}

              firebase.updateValue('player-variables/'+userId+'/dungeon', payload);

              setTimeout(()=>{
                floor.init(room_id , userId);
              }, 1000);
          }

      });
  }
}
