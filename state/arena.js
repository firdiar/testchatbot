//load library that u will use here
let firebase = require('../lib/firebaseLibs.js')
let send = require('../lib/sendMsg.js')
let gtion = require('../lib/gtionLibs.js')

//load other state related to this state
//Warning : Loading other state logic have to use delay
let defaultState = null
let floor = null
setTimeout(()=>{
  defaultState = require('../state/defaultState.js')
  floor = require('../state/floor.js')
}, 100);


module.exports = {
  init : function(room_id , userId){
      firebase.getValue('player-state/'+userId ,(value)=>{
          let code = gtion.stateToInt(value)
          if(code == 5){
              let arenaReff = gtion.stateToKey(value)[1]
              this.mainGreetSinggle(room_id , userId, arenaReff );
          }else{
              this.initBattle(room_id , userId);
          }
      });
  },
  initBattle : function(room_id ,userId){
      send.Txt_Prt(room_id , "Battle begin!");
      let refPath = userId
      firebase.setValue('player-state/'+userId , ('arena-'+refPath));
      //refPath = refPath;
      let counter = 0;
      firebase.getValue('player/'+userId+'/character' , (character)=>{
          character.status.name = character.name;
          character.status.userId = userId;
          character.status.room_id = character.room_id;
          character.status.ready = false;
          character.status.target = '';
          firebase.setValue('arena/'+refPath+'/player/'+userId , character.status);
          counter ++;
          if(counter == 2){
              this.mainGreet(refPath);
          }
      })

      firebase.getValue('player-variables/'+userId+'/meet-enemy' , (meetEnemy) =>{
          let keys = gtion.getVariableKey(meetEnemy);
          for(let i = 0 ; i < keys.length; i++){
              if( meetEnemy[keys[i]].monsterPath == null)
                continue;

                firebase.getValue(meetEnemy[keys[i]].monsterPath , (monster)=>{
                    if(meetEnemy[keys[i]].monsterLevel != monster.baseLv)
                      monster.status = gtion.monsterAscension (monster.status , monster.baseLv,meetEnemy[keys[i]].monsterLevel )
                    monster.status.currentEnergy = monster.status.maxEnergy
                    monster.status.currentHp = monster.status.maxHp
                    monster.status.name = monster.name+" Lv."+meetEnemy[keys[i]].monsterLevel
                    monster.status.image = monster.image;
                    firebase.pushValue('arena/'+refPath+'/enemy' ,monster.status )
                    if(i == keys.length-3){
                        counter ++;
                        if(counter == 2){
                            this.mainGreet(refPath);
                        }
                    }
                })

          }
      })
  },
  showEnemy : function(room_id , arenaReff , callback){
      firebase.getValue('arena/'+arenaReff+'/enemy' , (monsters) =>{
          let keys = gtion.getVariableKey(monsters);
          let cards = []
          for(let i = 0 ; i <  keys.length; i++){
              let enemyData = monsters[keys[i]];
              let mStats = gtion.statusToString(enemyData.currentHp , enemyData.currentEnergy , enemyData.maxHp , enemyData.maxEnergy , enemyData.attack , enemyData.accuracy , enemyData.agility);
              cards.push( gtion.carouselPostbackData(enemyData .image, enemyData.name , mStats , [gtion.buttonPostbackData('Select as Target' , '/target'+keys[i])] ) );
          }
          let payloadC = {
              'cards' : cards
          }
          send.Carousel_Prt(room_id , payloadC , callback)
      })
  },
  showPossibleAction : function(room_id, userId, reffArena){
      firebase.getValue('arena/'+reffArena+'/player/'+userId , (player) =>{
          let options = [ gtion.buttonPostbackData('Basic Atk ('+player.attack+')' , '/attack') ,gtion.buttonPostbackData('Preview Status' , '/status') , gtion.buttonPostbackData('Retreat (Penalty 5% Max Exp)' , '/back')  ]
          let payload = {
              'text': "Pilih aksi-mu!",
              'buttons': options
          }
          send.Btn_Prt(room_id,payload)
      });
  },
  mainGreet : function(reffArena){
      firebase.getValue('arena/'+reffArena , (arena)=>{
          let players = arena.player;
          if(arena.enemy == null){
              firebase.deleteValue('arena/'+reffArena);
              this.backToFloor(players);
          }else{
              let playerKey = gtion.getVariableKey(players);
              for(let i = 0; i < playerKey.length; i++){
                  console.log("MaINGreet - ",i);
                  //this.mainGreetSinggle(players[playerKey[i]].room_id , players[playerKey[i]].userId , reffArena);
                  send.Txt_Prt(players[playerKey[i]].room_id , "Giliran-mu");
                  this.showEnemy(players[playerKey[i]].room_id , reffArena, ()=>{
                      this.showPossibleAction(players[playerKey[i]].room_id ,players[playerKey[i]].userId, reffArena);
                  })
              }
          }
      })
  },
  mainGreetSinggle:function(room_id,userId,reffArena){
      console.log(room_id,userId,reffArena);
      send.Txt(room_id , "Giliran-mu",()=>{
          this.showEnemy(room_id , reffArena, ()=>{
              this.showPossibleAction(room_id ,userId, reffArena);
          })
      });
  },
  mainFunction : function (payloadBody , state){ // get value from database and return to callback
    // action when user send message
    let ansKeys = gtion.stateToKey(payloadBody.originalMsg)
    let arenaReff = gtion.stateToKey(state)[1];
    switch (ansKeys[0]) {
        case '/attack' :
            console.log("comming");
            firebase.setValue('arena/'+arenaReff+'/player/'+payloadBody.userId+'/ready' , true)
            firebase.getValue('arena/'+arenaReff , (arena)=>{
                let target = arena.player[payloadBody.userId].target;
                if(arena.enemy[target] == null)
                    target = this.getRandomKey(arena.enemy);

                let result = this.implementDamage(arena.player[payloadBody.userId] , arena.enemy[target]  ,arena.player[payloadBody.userId].attack );
                if(result.isDodged)
                    send.Txt(payloadBody.room_id , "'"+arena.enemy[target].name+"' berhasil menghindari serangan-mu");
                else{
                    if(result.damageReciver.currentHp > 0)
                        firebase.setValue( 'arena/'+arenaReff +"/enemy/"+target+'/currentHp' , result.damageReciver.currentHp )
                    else
                        firebase.deleteValue('arena/'+arenaReff +"/enemy/"+target);

                    send.Txt(payloadBody.room_id , "'"+arena.enemy[target].name+"' menderita "+arena.player[payloadBody.userId].attack+" damage" , ()=>{
                        this.checkEnemyTurn(arenaReff)
                    });
                    //setTimeout(()=>{} , 500)
                }
            })
            break;
        case '/target':
            firebase.setValue('arena/'+arenaReff+'/player/'+payloadBody.userId+'/target' , '-'+ansKeys[1])
            firebase.getValue('arena/'+arenaReff+'/enemy/-'+ansKeys[1] , (value)=>{
                if(value == null)
                    send.Txt(payloadBody.room_id , "Target saat ini, 'Random Enemy'");
                else
                    send.Txt(payloadBody.room_id , "Target saat ini, '"+value.name+"'");
            })
            break;
        case '/back':
            floor.init(payloadBody.room_id , payloadBody.userId);
            firebase.deleteValue('player-variables/'+payloadBody.userId+'/meet-enemy');
            firebase.deleteValue('arena/'+payloadBody.userId);
            break;
        default:
            defaultState.unknownAnswer(payloadBody.room_id);
    }
    },
    getRandomKey : function(list){
        let keys = gtion.getVariableKey(list);
        return keys[Math.floor(Math.random()*keys.length)]
    },
    implementDamage:function(damageGiver , damageReciver , damage){
        let isDodged = true
        if(Math.random() >  Math.min(0.9 , (damageReciver.agility/damageGiver.accuracy) ) ){
            damageReciver.currentHp -= damage;
            isDodged = false;
        }
        return {damageGiver:damageGiver , damageReciver : damageReciver, isDodged:isDodged}
    },
    checkEnemyTurn : function(arenaReff ){
        firebase.getValue('arena/'+arenaReff , (arena)=>{

            let playerKey = gtion.getVariableKey(arena.player);
            let isEnemyTurn = true;
            for(let i = 0; i < playerKey.length; i++){
                if(arena.player[playerKey[i]].ready == false && arena.player[playerKey[i]].currentHp > 0){
                    isEnemyTurn = false;
                    break;
                }
            }

            if(isEnemyTurn){
                this.sendMessageBrodcast(arena.player , "Giliran Musuh")
                let enemyKey = gtion.getVariableKey(arena.enemy);
                for(let i = 0; i < enemyKey.length; i++){
                    let random = playerKey[Math.floor(Math.random()*playerKey.length)]
                    let result = this.implementDamage(arena.enemy[enemyKey[i]] , arena.player[random] ,arena.enemy[enemyKey[i]].attack )
                    if(result.isDodged)
                        this.sendMessageBrodcast(arena.player , "'"+arena.player[random].name +"' berhasil menghindari serangan "+arena.enemy[enemyKey[i]].name)
                    else{
                        if(result.damageReciver.currentHp > 0){
                            firebase.setValue( 'arena/'+arenaReff +"/player/"+random+'/currentHp' , result.damageReciver.currentHp )
                        }else {
                            firebase.deleteValue('arena/'+arenaReff +"/player/"+random);
                            send.Txt(arena.player[random].room_id , 'Kamu telah mati, menteleportasi ke kota' , ()=>{
                                firebase.setValue('player-state/'+arena.player[random].userId , 'town')
                                // do something else
                            });
                        }
                        this.sendMessageBrodcast(arena.player , "'"+arena.player[random].name +"' menderita "+arena.enemy[enemyKey[i]].attack+" damage")
                        //send.Txt(payloadBody.room_id , "'"+arena.enemy[target].name+"' menderita "+arena.player[payloadBody.userId].attack+" damage");
                    }
                }
                this.mainGreet(arenaReff);
            }
        })
    },
    sendMessageBrodcast:function(players , msg){
        let playerKey = gtion.getVariableKey(players);
        for(let i = 0; i < playerKey.length; i++){
            send.Txt(players[playerKey].room_id , msg);
        }
    },
    backToFloor:function(players){
        let playerKey = gtion.getVariableKey(players);
        for(let i = 0; i < playerKey.length; i++){
            send.Txt(players[playerKey].room_id , "Battle dimenangkan" , ()=>{floor.init(players[playerKey[i]].room_id , players[playerKey[i]].userId  );});
        }
    }
}
