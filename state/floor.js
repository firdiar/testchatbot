//load library that u will use here
let firebase = require('../lib/firebaseLibs.js')
let send = require('../lib/sendMsg.js')
let gtion = require('../lib/gtionLibs.js')

//load other state related to this state
//Warning : Loading other state logic have to use delay
let defaultState = null;
let dungeon = null;
let onMeetEnemy = null;
setTimeout(()=>{
  defaultState = require('../state/defaultState.js')
  dungeon = require('../state/dungeon.js')
  onMeetEnemy = require('../state/onMeetEnemy.js');
}, 100);




module.exports = {
  init : function(room_id , userId){
      //message when enter this state

      this.askDecision(room_id , userId);
  },
  mainFunction : function (payloadBody , state ){ // get value from database and return to callback
    // action when user send message
    switch (payloadBody.msg) {
        case '/back':
            send.Txt(payloadBody.room_id , "Maninggalkan Lantai..." , ()=> {dungeon.init(payloadBody.room_id , payloadBody.userId);});
            break;
        case '/explore':
            this.explore(payloadBody.room_id, payloadBody.userId);
            break;
        case '/fight':{
                if(state == 'floor-1') this.fightBoss(payloadBody.room_id, payloadBody.userId);
                else send.Txt(payloadBody.room_id , "Lakukan eksplorasi hingga 100% sebelum melawan Boss");
            }
            break;
        default:
            defaultState.unknownAnswer(payloadBody.room_id);
    }
  },
  explore:function(room_id , userId){
      firebase.getValue('player-variables/'+userId+'/dungeon' , (dungeonValue)=>{
          let firebaseFloorPath = 'floor/'+ gtion.numberToID( dungeonValue.floor  , "floor-" , "0000");
          firebase.getValue(firebaseFloorPath , (currentFloor)=>{
              let explorationIncrement = Math.floor(Math.random() * currentFloor.exploration.max) + currentFloor.exploration.min;

              firebase.updateValue('player-variables/'+userId+'/dungeon' , { progress : Math.min(100 , dungeonValue.progress+explorationIncrement) });

              if(Math.random() < 0.8){//80% probability to fight enemy
                  let enemyCount =  Math.floor(Math.random() * currentFloor.enemyCount.max) +currentFloor.enemyCount.min;
                  let xpGain = (Math.floor(Math.random() * currentFloor.xp.max) + currentFloor.xp.min) * enemyCount;

                  let monsterKeys = gtion.getVariableKey( currentFloor.monsterList );
                  let monsterList = [];
                  for(let i = 0 ; i < enemyCount; i++){
                      monsterList.push( monsterKeys[Math.floor(Math.random()*monsterKeys.length)] );
                  }
                  this.fight(room_id , userId , xpGain , monsterList ,currentFloor, false)
              }else{
                  this.askDecision(room_id , userId);
              }



          })
      })
  },
  fightBoss:function(room_id , userId){
      firebase.getValue('player-variables/'+userId+'/dungeon' , (dungeonValue)=>{
          let firebaseFloorPath = 'floor/'+gtion.numberToID( dungeonValue.floor , "floor-" ,"0000" );
          firebase.getValue(firebaseFloorPath , (currentFloor)=>{
              let monsterList = gtion.getVariableKey( currentFloor.boss );
              this.fight(room_id , userId , currentFloor.xp.boss , monsterList , currentFloor , true)
          })
      })
  },
  fight : function(room_id , userId , xpGain , monsterId , currentFloor , isBoss){

      firebase.updateValue('player-variables/'+userId+'/meet-enemy' , {xpGain, isBoss} )
      let monsterData = []
      let mList = null;
      if(isBoss){
          mList = currentFloor.boss;
      }else{
          mList = currentFloor.monsterList;
      }
      for (let i = 0 ; i <  monsterId.length; i++){
          let monsterLevel = mList[monsterId[i]];
          let monsterPath = 'monster/'+monsterId[i];
          monsterData.push({monsterPath , monsterLevel})
          firebase.pushValue('player-variables/'+userId+'/meet-enemy' , {monsterPath , monsterLevel})
      }

      firebase.setValue('player-state/'+userId , 'onMeetEnemy');

      onMeetEnemy.askDecision(room_id , userId , xpGain ,monsterData);
  },
  askDecision : function(room_id , userId){
      firebase.getValue('player-variables/'+userId+'/dungeon' , (dungeon)=>{
          let options = [ gtion.buttonPostbackData('Explore This Floor' , '/explore') ]

          if(dungeon.progress >= 100){
              options.push(gtion.buttonPostbackData('Fight Boss' , '/fight'))
              firebase.setValue('player-state/'+userId , 'floor-1');
          }else{
              firebase.setValue('player-state/'+userId , 'floor');
          }

          if(dungeon.highestFloor == dungeon.floor){
              options.push(gtion.buttonPostbackData('Back and Reset Explore' , '/back'))
          }else{
              options.push(gtion.buttonPostbackData('Back' , '/back'))
          }

          let payload = {
              'text': "𝗙𝗹𝗼𝗼𝗿 - "+dungeon.floor+"\nEksplorasi : "+dungeon.progress+"%\nApa yang ingin kamu lakukan?",
              'buttons': options
          }
          send.Btn(room_id,payload)
      })
  }
}
