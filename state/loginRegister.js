let firebase = require('../lib/firebaseLibs.js')
let send = require('../lib/sendMsg.js')
let gtion = require('../lib/gtionLibs.js')

//Warning : Loading other state logic have to use delay
let greeting = null
let town = null
setTimeout(()=>{
  greeting = require('../state/greeting.js')
  town = require('../state/town.js')
}, 100);


function generateDefaultUser(username , password , room_id){
    return {
              account: { password: password, username: username },
              character: {
                room_id : room_id ,
                diamond: 0,
                experience: 0,
                gold: 100,
                level: 1,
                name: username,
                ascension : 5,
                status: {
                  accuracy: 100, // up 10
                  agility: 10, // up 1
                  attack: 10, // up 1
                  currentEnergy: 10,
                  currentHp: 100,
                  maxEnergy: 10, // up 1
                  maxHp: 100 // up 10
                }
              }
            }
}


function generateDefaultBelongings(){
    return {
       dungeon: {
         highestFloor : 1,
         floor: 1,
         progress: 0
       },
       /*equipment: {
         armor: 'none',
         weapon: 'none'
     },*/
       item: {
         count: 0
       },
       /*skill: {
         skill1: 'none',
         skill2: 'none',
         skill3: 'none'
     },*/
        spendPoint : {
          accuracy: 0,
          agility: 0,
          attack: 0,
          energy: 0,
          hp: 0
        }
     }
}

module.exports = {
	initLogin:function(room_id, userId){
        firebase.setValue('player-state/'+userId , 'login-1')
		send.Txt(room_id , "Masukan Username");
	},
	initRegist:function(room_id, userId){

		firebase.getValue('player/'+userId+'/account/username' , (username)=>{

               if(username != null){
                   firebase.setValue('player-state/'+userId , 'regist-1-exist')
                   this.cancelRequest(room_id,'PERINGATAN : Kamu telah memiliki akun, registrasi akun akan menghapus akun lama-mu',()=>{
                       send.Txt(room_id , "Masukan Username");
                   });
               }else{
                   firebase.setValue('player-state/'+userId , 'regist-1')
                   send.Txt(room_id , "Masukan Username");
               }


		});
	},
	mainFunction : function (payloadBody, state){ // get value from database and return to callback

        if(payloadBody.msg === '/cancel'){
            this.backToGreet(payloadBody.room_id , payloadBody.userId , payloadBody.name, 0);
            return;
        }

        switch (state) {
            case "login-1":
                this.login1(payloadBody.room_id,  payloadBody.userId , payloadBody.name , payloadBody.msg);
                break;
            case "login-2":
                this.login2(payloadBody.room_id,  payloadBody.userId , payloadBody.name , payloadBody.msg);
                break;
            case "regist-1-exist":
                this.cancelRequest(payloadBody.room_id,'PERINGATAN : Kamu telah memiliki akun, registrasi akun akan menghapus akun lama-mu',()=>{
                    this.regist1(payloadBody.room_id,  payloadBody.userId, payloadBody.msg);
                });
                break;
            case "regist-1":
                this.regist1(payloadBody.room_id,  payloadBody.userId, payloadBody.msg);
                break;
            case "regist-2":
                this.regist2(payloadBody.room_id,  payloadBody.userId, payloadBody.msg);
                break;
        }

	},
	cancelRequest:function(room_id , msg , callback){
        let payload = {
            'text': msg,
            'buttons': [gtion.buttonPostbackData('Cancel' , '/cancel')]
        }
		send.Btn(room_id,payload , callback)
	},
    login1:function(room_id, userId , username , msg){
        firebase.searchValue('player' , 'account/username' , msg , (value)=>{
           if(value == null){//jika username tidak ada maka kembali
                 send.Txt(room_id , "Username tidak ditemukan",()=>{this.backToGreet(room_id , userId , username, 0);});
           }else{//jika username ada maka lanjutkan
                 send.Txt(room_id , "Masukan Password");
                 gtion.setUserVariables( userId ,'loginID', gtion.getVariableKey(value)[0])
                 firebase.setValue('player-state/'+userId, 'login-2')
           }
        })
    },
    login2:function(room_id,userId,username,msg){
        gtion.getUserVariables(userId, 'loginID' , true , (userID)=>{
            firebase.getValue('player/'+userID+'/account/password' , (password)=>{
                if(password == msg){
                  //password correct
                  this.loginSuccess(room_id ,userId , userID)
                }else{
                  //password incorrect
                  this.loginFailed(room_id , userId , username);
                }
            })
       })
   },
   loginSuccess:function(room_id , nowId , prevId){
       if(nowId != prevId){
         //user ganti device
         send.Txt(room_id , "Anda login melalui device berbeda sebelumnya, Melakukan konversi...");
         firebase.getValue('player/'+prevId , (userData)=>{
             userData.account.room_id = room_id;
             firebase.setValue('player/'+nowId,userData);
             firebase.deleteValue('player/'+prevId);
             firebase.getValue('player-variables/'+prevId , (userData)=>{
                 let temp = { dungeon: userData.dungeon , equipment : userData.equipment , item : userData.item , skill:userData.skill , spendPoint:userData.spendPoint }
                 firebase.setValue('player-variables/'+nowId,temp);
                 firebase.deleteValue('player-variables/'+prevId);

                 send.Txt(room_id , "Konversi berhasil, Login Berhasil!" , ()=>{
                      town.init(room_id , nowId);
                 });
             });
         });

       }else{
         // login success
         send.Txt(room_id , "Login berhasil!",()=>{
              town.init(room_id , nowId);
         });
         firebase.setValue('player/'+nowId+'/account/room_id' , room_id);
       }


   },
   loginFailed:function(room_id , userId , username){
       send.Txt(room_id , "Login gagal, Password salah");
       this.backToGreet(room_id , userId , username, 500);
   },
   regist1:function(room_id,userId,msg){
       firebase.searchValue('player' , 'account/username' , msg , (value)=>{
           if(value == null){
               gtion.setUserVariables(userId ,'registUsername', msg)
               firebase.setValue('player-state/'+userId , 'regist-2')
               send.Txt(room_id , "Masukan Password")
            }else{
               send.Txt(room_id , "Maaf Username telah digunakan\n.\nMasukan Username");
            }
       })
   },
   regist2:function(room_id,userId,msg){
       gtion.getUserVariables( userId ,'registUsername', true , (username)=>{
           firebase.setValue('player/'+userId , generateDefaultUser(username , msg , room_id) )
           firebase.updateValue('player-variables/'+userId , generateDefaultBelongings())

       })
       send.Txt(room_id , "Registrasi Berhasil!",()=>{
            town.init(room_id , userId);
       });

   },
   backToGreet:function(room_id , userId , username, delay){
       if(delay > 0){
           setTimeout(()=>{
               greeting.init(room_id , userId, username);
           }, delay);
       }else{
           greeting.init(room_id , userId, username);
       }
   }
}
