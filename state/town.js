//load library that u will use here
let firebase = require('../lib/firebaseLibs.js')
let send = require('../lib/sendMsg.js')
let gtion = require('../lib/gtionLibs.js')

//load other state related to this state
//Warning : Loading other state logic have to use delay
let defaultState = null
let dungeon = null
setTimeout(()=>{
    defaultState = require('../state/defaultState.js')
    dungeon = require('../state/dungeon.js')
}, 100);


module.exports = {
  init : function(room_id , userId){
      //message when enter this state
      firebase.setValue('player-state/'+userId , 'town')
      let pesan = '𝗧𝗼𝘄𝗻\nSelamat datang di Town! apa yang ingin kamu lakukan'
      let payload = {
          'text': pesan,
          'buttons': [
              gtion.buttonPostbackData('Dungeon','/dungeon'),gtion.buttonPostbackData('Celestian Palace','/celestial'),gtion.buttonPostbackData('Shop','/shop')
          ]
      }
      send.Btn(room_id,payload)
  },
  mainFunction : function (payloadBody){ // get value from database and return to callback
    // action when user send message
    switch (payloadBody.msg) {
        case '/dungeon':
            dungeon.init(payloadBody.room_id , payloadBody.userId );
            break;
        case '/celestial':
            send.Txt(payloadBody.room_id,"Fitur 'Celestial Palace' belum terbuka")
            break;
        case '/shop':
            send.Txt(payloadBody.room_id,"Fitur 'Shop' belum terbuka")
            break;
        default:
            defaultState.unknownAnswer(payloadBody.room_id);
    }
  }
}
