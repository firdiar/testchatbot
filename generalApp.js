//Content Library
//let send = require('./lib/sendMsg.js')
let firebase = require('./lib/firebaseLibs.js')
let gtion = require('./lib/gtionLibs.js')

//logicProgram
let defaultState = require('./state/defaultState.js')
let greeting = require('./state/greeting.js')
let loginRegist = require('./state/loginRegister.js')
let town = require('./state/town.js')
let dungeon = require('./state/dungeon.js')
let floor = require('./state/floor.js')
let onMeetEnemy = require('./state/onMeetEnemy.js');
let arena= require('./state/arena.js');

function onMessageAccepted(payloadBody , state){

    switch (gtion.stateToInt(state)) {
        case 0:
            loginRegist.mainFunction(payloadBody , state);
            break;
        case 1:
            //dungeon.init(payloadBody.room_id , payloadBody.userId );
            town.mainFunction(payloadBody);
            break;
        case  2:
            dungeon.mainFunction(payloadBody , state);
            break;
        case  3:
            floor.mainFunction(payloadBody , state);
            break;
        case 4:
            onMeetEnemy.mainFunction(payloadBody);
            break;
        case 5:
            arena.mainFunction(payloadBody, state);
            break;
        default:
            greeting.mainFunction(payloadBody);
    }

}

module.exports = {
    Chatbot:function(payloadBody){
        switch (payloadBody.msg) {
            case '/help':
                defaultState.askHelp( payloadBody.room_id );
                break;
            case '/menu':
                defaultState.menu(payloadBody);
                break;
            case '/status':
                defaultState.status(payloadBody);
                break;
            default:
                firebase.getValue_Payload('player-state/'+ payloadBody.userId ,  payloadBody , onMessageAccepted );
        }
    }
}
