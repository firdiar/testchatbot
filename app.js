'use strict';

//Host
const express = require('express');
const bodyParser = require('body-parser');

const generalApp = require('./generalApp.js')

//Init Server
const server = express();
server.use(bodyParser.urlencoded({
    extended: true
}));
server.use(bodyParser.json());

server.post('/',(req,res)=>{
    //konversi pesan yang mengandung huruf kapital menjadi huruf kecil

    let payloadBody = {room_id : res.req.body.chat_room.qiscus_room_id , userId : res.req.body.from.id , originalMsg :res.req.body.message.text  ,msg : res.req.body.message.text.toLowerCase() , name:res.req.body.from.fullname}
    //bot.deleteMessage( payloadBody.room_id , callbackQuery.message.message_id);

    generalApp.Chatbot(payloadBody);

})


server.listen((process.env.PORT || 3000), () => {
    console.log("Server is up and running : "+`${process.env.PORT}`);
});
