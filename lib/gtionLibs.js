let firebase = null
let send = null

setTimeout(()=>{
  firebase = require('../lib/firebaseLibs.js')
  send = require('../lib/sendMsg.js')
}, 100);

const dictionaryState = {
    "null" : -1,
    "login" : 0,
    "regist" : 0,
    "town" : 1,
    "dungeon" : 2,
    "floor" : 3,
    "onMeetEnemy" : 4,
    "arena" : 5,
}

function calculateAscensionPoint(level){
    let n = Math.floor(level/10)
    let bonus = (level - (Math.floor(level/10)*10)) * (n+1)
    return ((n/2) * (2 + (n-1)))*10 + bonus;
}

module.exports = {
    getVariableKey : function(variable){
        let result = []
        for(let key in variable)
            result.push(key);
        return result;
    },
    stateToInt:function (state) {
        return dictionaryState[this.stateToKey(state)[0]];
    },
    stateToKey:function (state) {
        if(state == null)
            return 'null';

        let key = state.split("-");
        return key;
    },
    setUserVariables:function(userID , key , value){
        firebase.setValue('player-variables/'+userID+'/'+key , value)
    }
    ,
    getUserVariables:function(userID , key  , deletAfter , callback){
        firebase.getValue('player-variables/'+userID +'/'+key , (value)=>{
          callback(value);
          if(deletAfter)
            firebase.deleteValue('player-variables/'+userID +'/'+key);
        });

    },
    buttonPostbackData : function(label , postback){
        return {
          'label': label,
          'type': 'postback',
          'postback_text' : postback,
          'payload': {
              'url': '#',
              'method': 'get',
              'payload': 'null'
          }
        }
    },
    carouselPostbackData : function(image , title , description , buttons){
            return {
                        'image' : image,
                        'title' : title,
                        'description' : description,
                        'buttons' :  buttons
                    }

    },

    fullStatusToString : function(name ,hp , energy , maxHp , maxEnergy,attack, accuracy , agility){
        return "Name\t: "+name+"\n" + this.statusToString(hp , energy , maxHp , maxEnergy,attack, accuracy , agility);
    },
    statusToString : function(hp , energy , maxHp , maxEnergy,attack, accuracy , agility){
        return "Health\t: "+hp+"/"+maxHp+"\nEnergy\t: "+energy+"/"+maxEnergy+"\nAttack\t: "+attack+"\nAccuracy\t: "+accuracy+"\nAgility\t: "+agility
    },
    numberToID:function(floorNumber , prefix , pad){
        var str = "" + floorNumber
        //var pad = "0000"
        var ans = prefix+pad.substring(0, pad.length - str.length) + str
        return ans;
    },
    getRequireLevelUpExp:function(level){
        return Math.pow(level, 2)*10
    },
    getRetreatPenalty:function(level){
        return Math.ceil(this.getRequireLevelUpExp(level) * 0.05); // 5% of current require level up
    },
    getDeathPenalty:function(level){
        return Math.ceil(this.getRequireLevelUpExp(level) * 1); // 100% of current require level up
    },
    increaseExperience:function(room_id ,userId , exp){
        firebase.getValue('player/'+userId+'/character' , (character)=>{
            let updateExp = character.experience + exp;
            let isLevelUp = false;
            let baseLv = character.level;
            while (this.getRequireLevelUpExp(character.level) <= updateExp) {
                updateExp -= this.getRequireLevelUpExp(character.level)
                character.level++;
                send.Txt(room_id , "Level meningkat!");
                isLevelUp = true;
            }
            if(isLevelUp){
                character.ascension += calculateAscensionPoint(character.level) - calculateAscensionPoint(baseLv);
                send.Txt(room_id , "Gunakan Point Ascension : "+character.ascension);
            }

            character.experience = updateExp;
            firebase.setValue('player/'+userId+'/character' , character);
        })
    },
    decreaseExperience:function(room_id ,userId , exp){
        firebase.getValue('player/'+userId+'/character' , (character)=>{
            let updateExp = character.experience - exp;
            let isLevelDown = false;
            let baseLv = character.level;
            while (0 > updateExp) {
                if(character.level <= 1){
                    updateExp = 0;
                    character.level = 1;
                    break;
                }
                character.level--;
                updateExp += this.getRequireLevelUpExp(character.level)
                isLevelDown = true;
                send.Txt(room_id , "Level menurun!");
            }
            character.ascension += calculateAscensionPoint(character.level) - calculateAscensionPoint(baseLv);
            character.experience = updateExp;
            if(isLevelDown && 0 > character.ascension){
                firebase.getValue('player-variables/'+userId+'/spendPoint' , (spendPoint)=>{
                    let result = this.autoDescension(character.status ,  character.ascension, spendPoint)
                    character.status = result.status;
                    character.ascension = 0;
                    firebase.setValue('player/'+userId+'/character' , character);
                    firebase.setValue('player-variables/'+userId+'/spendPoint' , result.spendPoint);
                })
            }else {
                firebase.setValue('player/'+userId+'/character' , character);
            }
        })
    },
    monsterAscension : function(status , baseLv , currentLv){
        let ascensionPoint = calculateAscensionPoint(currentLv) - calculateAscensionPoint(baseLv);
        //console.log(ascensionPoint , calculateAscensionPoint(currentLv) , calculateAscensionPoint(baseLv));
        if(ascensionPoint < 0){
            return this.autoDescension(status , ascensionPoint).status;
        }else if(ascensionPoint > 0){
            return this.autoAscension(status , ascensionPoint);
        }else{
            return status;
        }
    },
    autoAscension : function(status , ascensionPoint){
        while (0 < ascensionPoint) {
            let randomNumber = Math.floor(Math.random()*5);
            switch (randomNumber) {
                case 0 :
                status.accuracy += 10;
                break;
                case 1 :
                status.agility += 1;
                break;
                case 2 :
                status.attack += 10;
                break;
                case 3 :
                status.maxEnergy += 1;
                break;
                case 4 :
                status.maxHp += 10;
                break;
            }
            ascensionPoint--;
        }
        return status;
    },
    autoDescension : function(status , ascensionPoint , spendPoint){
        if(spendPoint == null)
            spendPoint = {accuracy: 10000,agility: 10000,attack: 10000,energy: 10000,hp: 10000}

        let keys = this.getVariableKey(spendPoint);
        while (0 > ascensionPoint && keys.length > 0) {
            let randomNumber = Math.floor(Math.random()*keys.length);
            if(spendPoint[keys[randomNumber]] <= 0){
                keys.splice(randomNumber, 1);
            }else{
                spendPoint[keys[randomNumber]]--;
                switch (keys[randomNumber]) {
                    case "accuracy" :
                    status.accuracy -= 10;
                    break;
                    case "agility" :
                    status.agility -= 1;
                    break;
                    case "attack" :
                    status.attack -= 10;
                    break;
                    case "energy" :
                    status.maxEnergy -= 1;
                    status.currentEnergy = status.maxEnergy;
                    break;
                    case "hp" :
                    status.maxHp -= 10;
                    status.currentHp =status.maxHp;
                    break;
                }
                ascensionPoint++;
            }
        }
        return {status , spendPoint};
    }
}
