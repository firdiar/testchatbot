//Firebase API
var admin = require("firebase-admin");
var serviceAccount = require("../lib/serviceAccountKey.json");
// Initialize the app with a service account, granting admin privileges
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://project-tower-adventure.firebaseio.com"
});



// As an admin, the app has access to read and write all data, regardless of Security Rules
var firebaseDb = admin.database();



module.exports = {//reff : https://firebase.google.com/docs/database/admin/start
  getValue : function (path , callback){ // get value from database and return to callback
    var ref = firebaseDb.ref(path);
    ref.once("value", function(snapshot) {
      callback(snapshot.val());
    });
  }
  ,
  getValue_Payload : function (path , payload , callback){ // get value from database and return to callback
    var ref = firebaseDb.ref(path);
    ref.once("value", function(snapshot) {
      callback(payload , snapshot.val());
    });
  }
  ,
  setValue : function(path , payload){
    var ref = firebaseDb.ref(path);
    ref.set(payload);
  }
  ,
  updateValue : function(path , payload){
    var ref = firebaseDb.ref(path);
    ref.update(payload);
  }
  ,
  deleteValue : function(path){
    var ref = firebaseDb.ref(path);
    ref.set({});
  }
  ,
  pushValue : function(path, payload){
    firebaseDb.ref(path).push().set(payload);
  },
  pushRef : function(path){
    return (firebaseDb.ref(path).push().key);
  },
  searchValue : function(path , childPath , value, callback){
    var ref = firebaseDb.ref(path);
    ref.orderByChild(childPath).equalTo(value).once("value", function(snapshot) {
      //console.log(snapshot.val());
      callback(snapshot.val());
    });
  }

}
