//if chataja

require('dotenv').config()
const token = `${process.env.access_token}`
const apiUrl = `${process.env.apiUrl}`

const axios = require('axios');


function setRawBody(token , room_id , type , payload){
  return {
              access_token: token,
              topic_id: room_id,
              type: type,
              payload: JSON.stringify(payload)
          }
}


module.exports = {
    Txt: (room_id , msg , callback) => {
        //let raw = setRawBody(token , room_id , 'buttons' ,null);
        //raw.comment = msg
        axios.post(apiUrl + 'post_comment', {
            access_token: token,
            topic_id: room_id,
            type: 'text',
            comment: msg
        }).then(callback)
    },
    Btn: (room_id, payload, callback) => {
        axios.post(apiUrl + 'post_comment', setRawBody(token , room_id , 'buttons' , payload) ).then(callback);
    },
    Carousel: (room_id, payload, callback) => {
        axios.post(apiUrl + 'post_comment', setRawBody(token , room_id , 'carousel' , payload)).then(callback);
    },
    Card: (room_id, payload, callback) => {
        axios.post(apiUrl + 'post_comment',  setRawBody(token , room_id , 'card' , payload)).then(callback);
    },
    File:(room_id,payload, callback)=>{
        axios.post(apiUrl + 'post_comment', setRawBody(token , room_id , 'file_attachment' , payload)).then(callback);
    },
    Txt_Prt: (room_id , msg ) => {
        //let raw = setRawBody(token , room_id , 'buttons' ,null);
        //raw.comment = msg
        axios.post(apiUrl + 'post_comment', {
            access_token: token,
            topic_id: room_id,
            type: 'text',
            comment: msg
        }).catch(function (error) {
      console.log( error.message)
    })
    },
    Btn_Prt: (room_id, payload) => {
        axios.post(apiUrl + 'post_comment', setRawBody(token , room_id , 'buttons' , payload) ).catch(function (error) {
      console.log( error)
    });
    },
    Carousel_Prt: (room_id, payload) => {
        axios.post(apiUrl + 'post_comment', setRawBody(token , room_id , 'carousel' , payload)).catch(function (error) {
      console.log(error)
    });
    },
    Card_Prt: (room_id, payload) => {
        axios.post(apiUrl + 'post_comment',  setRawBody(token , room_id , 'card' , payload));
    },
    File_Prt:(room_id,payload)=>{
        axios.post(apiUrl + 'post_comment', setRawBody(token , room_id , 'file_attachment' , payload));
    }

}


//if telegram
/*
let bot = require('../lib/atelegrambot.js')
function convertButtonToArray(buttons){
    let result = []
    for(let i= 0; i < buttons.length; i++){
        result.push([
          {
            text: buttons[i].label,
            callback_data:  buttons[i].postback_text,
          },
        ]);
    }
    return result;
}

function setRawBody(buttons){
    return {
            "reply_markup": {
                "inline_keyboard": convertButtonToArray(buttons),

            },
        }
}
module.exports = {
    Txt: (room_id , msg) => {
        bot.sendMessage(room_id, msg);
    },
    Btn: (room_id, payload) => {
        bot.sendMessage(room_id, payload.text , setRawBody(payload.buttons));
    }
}
*/
